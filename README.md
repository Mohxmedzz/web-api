# web-api
Is a Spring Boot application with database connection that can do CRUD operations about movies, franchises and characters from movies.

###    1 Background
Java Spring Boot application with PostgreSql DB for an assignment at Noroff Re-Skilling program. 

###    2 Install
To set up the development environment you will need Intellij with JDK installed (in this application jdk 17 is being used),
git clone the project as following:
git clone https://gitlab.com/Mohxmedzz/web-api.git
Postgresql was used as DB for this project.

###    3 Run
Then open up a browser while running your application and visist http://localhost:8080/swagger-ui/index.html to play around with the application's functions. It's only CRUD functions that handles movies, movie characters and franchises.


## Usage

In your application.properties file in src/main/resources add your DB connection and user&password
spring.datasource.url="the url to the database you created"
spring.datasource.username="Your username"
spring.datasource.password="Your password"

Then open up a browser while running your application and visit http://localhost:8080/swagger-ui/index.html to play around with the application's functions.


## Contributing
[Mohamed Abdel Monem (@Mohxmedzz)](@Mohxmedzz)
[Patrick Hajiyahya (@moshixd)](@moshixd)
