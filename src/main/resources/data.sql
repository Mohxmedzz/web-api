insert into franchise (name, description)
values ('Rush Hour',
        'Rush Hour was a highly entertaining three-movie franchise running from 1998 to 2007. The series, which featured Jackie Chan as Inspector Lee and Chris Tucker as Detective Carter, was a hit at the box office.');

insert into franchise (name, description)
values ('Batman', 'The Batman film franchise follows various incarnations of the character created by DC Comics.');

insert into franchise (name, description)
values ('John Wick', 'A former hitman who is forced back into the criminal underworld he had abandoned.');

-- Movies without franchise can use this row with id 4
-- when adding movies by putting in the parameter franchise_id as 4
insert into franchise (name, description)
values ('No franchise', 'Movies without franchise');

insert into movie ( title, year, director_name, genre, picture, trailer, franchise_id)
values (
           'Rush Hour',
           1998,
           'Brett Ratner',
           'Action, Comedy, Crime',
           'https://m.media-amazon.com/images/M/MV5BYWM2NDZmYmYtNzlmZC00M2MyLWJmOGUtMjhiYmQ2OGU1YTE1L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg',
           'https://www.youtube.com/watch?v=JMiFsFQcFLE&ab_channel=RottenTomatoesClassicTrailers', null
       );

insert into movie (title, year, director_name, genre, picture, trailer, franchise_id)
values (
    'The Dark Knight',
    2008,
    'Christopher Nolan',
    'Action, Crime, Drama',
    'https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_.jpg',
    'https://www.imdb.com/video/vi324468761/?playlistId=tt0468569&ref_=tt_ov_vi', null);

insert into movie (title, year, director_name, genre, picture, trailer, franchise_id)
values ('John Wick',
        2014,
        'Chad Stahelski',
        'Action, Crime, Thriller',
        'https://img.fruugo.com/product/6/55/162201556_max.jpg',
        'https://www.youtube.com/watch?v=C0BMx-qxsP4',
        null);

insert into movie_character (full_name, alias, gender, picture)
values ('Yan Naing Lee',
    'Lee',
    'Male',
    'https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Jackie_Chan_in_The_Fearless_Hyena.jpg/640px-Jackie_Chan_in_The_Fearless_Hyena.jpg');

insert into movie_character (full_name, alias, gender, picture)
VALUES ('Bruce Wayne',
    'The Dark Knight',
    'Male',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSvG-nmFtmgKYZY3oTC4iDaqGQboiNe0qPpA&usqp=CAU'
    );

insert into movie_character (full_name, alias, gender, picture)
values ('John Wick',
    'The Boogeyman',
    'Male',
    'https://tse2.mm.bing.net/th/id/OIP.j_-WIO7Bj4two_l-YCu9ywHaHZ?pid=ImgDet&rs=1'
    );

insert into movie_characters_starring (movie_id, movie_character_id)
values (2,2);

insert into movie_characters_starring (movie_id, movie_character_id)
values (3,1);

insert into movie_characters_starring (movie_id, movie_character_id)
values (1,3);

alter table movie
drop constraint fke6bia0emn9rgc6498fspevei;

alter table movie
    add constraint fke6bia0emn9rgc6498fspevei
        foreign key (franchise_id) references franchise
            on update set null on delete set null;

