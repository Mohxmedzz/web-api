package com.moshi.webapi.controller;

import com.moshi.webapi.apiErrorResponse.ApiErrorResponse;
import com.moshi.webapi.mapper.FranchiseMapper;
import com.moshi.webapi.models.DTO.franchise.FranchiseDTO;
import com.moshi.webapi.models.Franchise;
import com.moshi.webapi.models.Movie;
import com.moshi.webapi.services.FranchiseService.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@RestController
@RequestMapping("api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;


    // Metadata for GET all franchises operation
    @Operation(summary = "Gets all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchises successfully retrieved",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchises not found",
                    content ={ @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    } )
    })
    @GetMapping // GET: localhost:8080/api/v1/franchises
    public ResponseEntity findAll() {
        Collection<FranchiseDTO> franchises = franchiseMapper.franchisesToFranchiseDTO(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchises);
    }

    // Metadata for GET a given franchise by id operation
    @Operation(summary = "Gets a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchise successfully retrieved",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with the supplied ID",
                    content = @Content)
    })
    @GetMapping("/{id}")// GET: localhost:8080/api/v1/franchises/1
    public ResponseEntity findById(@PathVariable Long id) {
        FranchiseDTO franchise = franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(franchise);
    }

    // method for finding franchise by given name
    @GetMapping("search") // GET: localhost:8080/api/v1/franchises/search?name=rush_hour
    public ResponseEntity<Collection<Franchise>> findByName(@RequestParam String name) {
        return ResponseEntity.ok(franchiseService.findAllByName(name));
    }

    // Metadata for Post a new added franchise operation
    @Operation(summary = "Adds a franchise")
    @PostMapping
    public ResponseEntity add(@RequestBody Franchise fran) {
        Franchise newfran = franchiseService.add(fran);
        URI uri = URI.create("franchises/" + newfran.getId());
        return ResponseEntity.created(uri).build();
    }

    // Metadata for Put a new added franchise operation
    @Operation(summary = "Updates a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with the supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}") // PUT: localhost:8080/api/v1/franchises/1
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable Long id) {
        if(!Objects.equals(franchiseDTO.getId(), id))
            return ResponseEntity.badRequest().build();
        franchiseService.update(franchiseMapper.franchiseDtoToFranchise(franchiseDTO));
        return ResponseEntity.noContent().build();
    }
    @PutMapping("/{id}/updateMovie")
    public ResponseEntity<List<Movie>> updateMovieToFranchise(@PathVariable Long id, @RequestBody List<Long> movieId) {
        franchiseService.updateMovieToFranchise(id, movieId);
        return ResponseEntity.noContent().build();
    }



    // Metadata for Put a deleted franchise operation
    @Operation(summary = "Deletes a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with the supplied ID",
                    content = @Content)
    })
    @DeleteMapping("/{id}") // DELETE: localhost:8080/api/v1/franchises/1
    public ResponseEntity delete(@PathVariable Long id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
