package com.moshi.webapi.controller;

import com.moshi.webapi.mapper.CharacterMapper;
import com.moshi.webapi.models.DTO.character.CharacterDTO;
import com.moshi.webapi.models.MovieCharacter;
import com.moshi.webapi.services.MovieCharacterService.MovieCharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Objects;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/v1/characters")
public class MovieCharacterController {

    private final MovieCharacterService movieCharacterService;
    private final CharacterMapper characterMapper;

    @Operation(summary = "Gets all movie characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Characters successfully retrieved",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Characters not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity findAll() {
        Collection<CharacterDTO> characterDTO = characterMapper.movieCharactersToMovieCharactersDTO(movieCharacterService.findAll());
        return ResponseEntity.ok(characterDTO);
    }

    @Operation(summary = "Gets a movie character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Character successfully retrieved",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with the supplied ID",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        CharacterDTO characterDTO = characterMapper.movieCharacterToMovieCharacterDTO(movieCharacterService.findById(id));
        return ResponseEntity.ok(characterDTO);
    }

    @Operation(summary = "Adds a movie character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Character successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with the supplied ID",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody MovieCharacter movieCharacter) {
        MovieCharacter newMovieCharacter = movieCharacterService.add(movieCharacter);
        URI uri = URI.create("actors/" + newMovieCharacter.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a movie character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with the supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody CharacterDTO characterDTO, @PathVariable Long id) {
        if (!Objects.equals(characterDTO.getId(), id))
            return ResponseEntity.badRequest().build();
        movieCharacterService.update(
                characterMapper.movieCharacterDTOToMovieCharacter(characterDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a movie character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with the supplied ID",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        movieCharacterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
