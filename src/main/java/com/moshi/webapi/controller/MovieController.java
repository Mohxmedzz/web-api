package com.moshi.webapi.controller;

import com.moshi.webapi.mapper.MovieMapper;
import com.moshi.webapi.models.DTO.movie.MovieDTO;
import com.moshi.webapi.models.Movie;
import com.moshi.webapi.services.MovieService.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Objects;


@AllArgsConstructor
@RestController
@RequestMapping("api/v1/movies")
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;

    @Operation(summary = "Gets all movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies successfully retrieved",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movies not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity findAll() {
        Collection<MovieDTO> movieDTO = movieMapper.movieToMovieDTO(movieService.findAll());
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "Gets a movie by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Movie successfully retrieved",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with the supplied ID",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "Adds a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Movie successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with the supplied ID",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody Movie movie) {
        Movie newMovie = movieService.add(movie);
        URI uri = URI.create("Movies/" + newMovie.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with the supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody MovieDTO movieDTO, @PathVariable Long id) {
        if (!Objects.equals(id, movieDTO.getId()))
            return ResponseEntity.notFound().build();
        movieService.update(
                movieMapper.movieDTOToMovie(movieDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie sucessfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with the supplied ID",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
