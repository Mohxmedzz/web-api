package com.moshi.webapi.services.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.ALREADY_REPORTED, reason = "Character_ALREADY_EXIST")
public class CharacterAlreadyExistException extends Exception{

}