package com.moshi.webapi.services.MovieService;

import com.moshi.webapi.models.Movie;
import com.moshi.webapi.repository.MovieRepository;
import com.moshi.webapi.services.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService{
    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    /**
     * User gets a list of movies by movieId
     * @param id
     * @return
     */

    @Override
    public Movie findById(Long id) {
        return movieRepository.findById(id).orElseThrow(() -> new NotFoundException(id));
    }
    /**
     * User gets a list of all movies
     * @return
     */
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }
    /**
     * User can add a movie to the table
     * @return
     */
    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }
    /**
     * User can update existing data by using their id
     * @return
     */
    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }
    /**
     * This method deletes a movie by its id
     * @param id
     * @return
     */
    @Override
    public void deleteById(Long id) {
        movieRepository.deleteById(id);
    }

    @Override
    public boolean existsById(Long id) {
        return movieRepository.existsById(id);
    }
}
