package com.moshi.webapi.services.MovieService;

import com.moshi.webapi.models.Movie;
import com.moshi.webapi.services.CrudService;

import java.util.List;

public interface MovieService extends CrudService<Movie, Long> {
    boolean existsById(Long id);
}
