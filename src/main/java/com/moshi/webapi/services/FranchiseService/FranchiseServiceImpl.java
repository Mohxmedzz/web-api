package com.moshi.webapi.services.FranchiseService;

import com.moshi.webapi.models.Franchise;
import com.moshi.webapi.models.Movie;
import com.moshi.webapi.repository.FranchiseRepository;
import com.moshi.webapi.repository.MovieRepository;
import com.moshi.webapi.services.exceptions.MovieAlreadyExistException;
import com.moshi.webapi.services.exceptions.NotFoundException;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(MovieRepository movieRepository, FranchiseRepository franchiseRepository) {
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
    }
    /**
     * User can find any franchise by entering their id
     * @param id
     * @return
     */
    @Override
    public Franchise findById(Long id) {
        return franchiseRepository.findById(id).orElse(null);
    }
    /**
     * User gets a list of all franchise
     * @return
     */
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }
    /**
     * User can add new franchise
     * @param entity
     * @return
     */
    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }
    /**
     * User can update any existing franchise who is saved in the table by their id
     * @param entity
     * @return
     */
    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }
    /**
     * User can delete any existing franchise in the table by their id
     * @param id
     * @return
     */
    @Override
    public void deleteById(Long id) {
        franchiseRepository.deleteById(id);
    }
    /**
     * User can find any franchise by entering their name
     * @param name
     * @return
     */
    @Override
    public Collection<Franchise> findAllByName(String name) {
        return franchiseRepository.findAllByName(name);
    }
    /**
     * User can update movie to any existing franchise who is saved in the table by their franchiseId and movieId
     * @param franchiseId
     * @param movieId
     * @return
     */
    @SneakyThrows
    public void updateMovieToFranchise(Long franchiseId, List<Long> movieId) {
        Franchise franchise = franchiseRepository.findById(franchiseId).orElseThrow();
        List<Movie> movies = franchise.getMovieIds();
        boolean contains;
        for (long id : movieId) {
            contains = movies.stream().anyMatch(movie -> movie.getId() == id);
            if (!contains) {
                if (movieRepository.existsById(id)) {
                    Movie movie = movieRepository.findById(id).orElseThrow(() -> new NotFoundException(id));
                    movie.addFranchise(franchise);
                    franchise.updateMovieToFranchise(movie);
                } else {
                    throw new NotFoundException(id);
                }
            }
            if(contains){
                throw new MovieAlreadyExistException();
            }
        }
        franchiseRepository.save(franchise);
    }

    @Override
    public boolean existsById(Long id) {
        return movieRepository.existsById(id);
    }
}
