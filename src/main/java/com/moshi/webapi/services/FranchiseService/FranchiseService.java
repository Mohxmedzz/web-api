package com.moshi.webapi.services.FranchiseService;


import com.moshi.webapi.models.Franchise;
import com.moshi.webapi.services.CrudService;

import java.util.Collection;
import java.util.List;

public interface FranchiseService extends CrudService<Franchise, Long> {
    Collection<Franchise> findAllByName(String name);
    boolean existsById(Long id);

    void updateMovieToFranchise(Long id, List<Long> movieId);
}
