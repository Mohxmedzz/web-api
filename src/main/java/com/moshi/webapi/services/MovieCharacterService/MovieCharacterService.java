package com.moshi.webapi.services.MovieCharacterService;

import com.moshi.webapi.models.MovieCharacter;
import com.moshi.webapi.services.CrudService;

public interface MovieCharacterService extends CrudService<MovieCharacter, Long> {
    boolean exists(Long id);
}
