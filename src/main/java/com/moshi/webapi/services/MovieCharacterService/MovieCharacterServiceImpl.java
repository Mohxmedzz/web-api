package com.moshi.webapi.services.MovieCharacterService;

import com.moshi.webapi.models.MovieCharacter;
import com.moshi.webapi.services.exceptions.NotFoundException;
import com.moshi.webapi.repository.MovieCharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class MovieCharacterServiceImpl implements MovieCharacterService {
    private final MovieCharacterRepository movieCharacterRepository;
    /**
     * User can find any character by entering their id
     * @param id
     * @return
     */
    @Override
    public MovieCharacter findById(Long id) {
        return movieCharacterRepository.findById(id).orElseThrow(() -> new NotFoundException(id));
    }
    /**
     * User gets a list of all characters
     * @return
     */
    @Override
    public Collection<MovieCharacter> findAll() {
        return movieCharacterRepository.findAll();
    }
    /**
     * User can add new characters
     * @param movieCharacter
     * @return
     */
    @Override
    public MovieCharacter add(MovieCharacter movieCharacter) {
        return movieCharacterRepository.save(movieCharacter);
    }
    /**
     * User can update any existing character who is saved in the table by their id
     * @param movieCharacter
     * @return
     */
    @Override
    public MovieCharacter update(MovieCharacter movieCharacter) {
        return movieCharacterRepository.save(movieCharacter);
    }
    /**
     * User can delete any existing character in the table by their id
     * @param id
     * @return
     */
    @Override
    public void deleteById(Long id) {
        movieCharacterRepository.deleteById(id);
    }

    @Override
    public boolean exists(Long id) {
        return movieCharacterRepository.existsById(id);
    }
}
