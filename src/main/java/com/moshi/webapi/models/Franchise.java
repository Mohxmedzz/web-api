package com.moshi.webapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "franchise")
public class Franchise {

    /**
     * A column for franchise table
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", length = 70)
    private String name;
    @Column(name = "description", length = 250)
    private String description;
    /**
     * Relationship between franchise and movies, one to many. (One franchise can have many movies)
     */
    @JsonIgnore
    @OneToMany(mappedBy = "franchise",fetch = FetchType.LAZY)
    private List<Movie> movieIds;

    public void updateMovieToFranchise(Movie movie){
        movieIds.add(movie);
    }

    @Override
    public String toString() {
        return "Franchise{" +
                "franchise_id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", movie=" +
                '}';
    }
}
