package com.moshi.webapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Movie {
    /**
     * Columns for movie table
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(name = "title", nullable = false, length = 70)
    private String title;
    @Column(name = "genre", nullable = false, length = 30)
    private String genre;

    @Column(name = "year", nullable = false)
    private int year;

    @Column(name = "director_name", nullable = false, length = 70)
    private String director_name;
    @Column(name = "picture", nullable = false, length = 250)
    private String picture;
    @Column(name = "trailer", nullable = false, length = 250)
    private String trailer;
    /**
     * Relationship between movies and franchise, many to one. (Many movies can have one franchise)
     */
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
    /**
     * New table with relationship for movies and character, many to many. (Many movies can have many character)
     */
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "movie_characters_starring",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_character_id"))
    private List<MovieCharacter> movieCharacters;
    public void addFranchise(Franchise franchise){
        setFranchise(franchise);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movie_id=" + id +
                ", title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", year=" + year +
                ", director_name='" + director_name + '\'' +
                ", picture='" + picture + '\'' +
                ", trailer='" + trailer + '\'' +
                ", movieCharacters=" + movieCharacters +
                ", franchise=" + franchise +
                '}';
    }
}
