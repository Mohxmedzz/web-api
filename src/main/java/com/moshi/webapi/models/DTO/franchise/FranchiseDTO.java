package com.moshi.webapi.models.DTO.franchise;

import lombok.Data;

import java.util.List;


@Data
public class FranchiseDTO {
    private Long id;
    private String name;
    private String description;
    private List<Long> movieIds;
}
