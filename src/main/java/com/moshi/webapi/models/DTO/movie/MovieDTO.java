package com.moshi.webapi.models.DTO.movie;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class MovieDTO {
    private Long id;
    private String title;
    private String genre;
    private int year;
    private String director_name;
    private String picture;
    private String trailer;
    private Long franchise;
    private List<Long> movieCharacters;

}
