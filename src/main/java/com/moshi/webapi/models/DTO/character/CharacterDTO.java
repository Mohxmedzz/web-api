package com.moshi.webapi.models.DTO.character;

import lombok.Data;

import java.util.List;

@Data
public class CharacterDTO {
    private Long id;
    private String full_name;
    private String alias;
    private String gender;
    private String picture;
    private List<Long> movieIds;

}
