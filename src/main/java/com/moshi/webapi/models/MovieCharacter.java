package com.moshi.webapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MovieCharacter {
    /**
     * Columns for movieCharacter table
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "full_name", nullable = false, length = 70)
    private String full_name;
    @Column(name = "alias", nullable = false, length = 70)
    private String alias;
    @Column(name = "gender", nullable = false, length = 10)
    private String gender;
    @Column(name = "picture", length = 250)
    private String picture;
    /**
     * New table with relationship for movies and character, many to many. (Many movies can have many character)
     */
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "movie_characters_starring",
            joinColumns = @JoinColumn(name = "movie_character_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id"))
    private List<Movie> movieIds;

    @Override
    public String toString() {
        return "Actor{" +
                "actor_id=" + id +
                ", full_name='" + full_name + '\'' +
                ", alias='" + alias + '\'' +
                ", gender=" + gender +
                ", picture='" + picture + '\'' +
                ", movies=" + movieIds +
                '}';
    }
}
