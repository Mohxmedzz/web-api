package com.moshi.webapi.mapper;

import com.moshi.webapi.models.DTO.character.CharacterDTO;
import com.moshi.webapi.models.Movie;
import com.moshi.webapi.models.MovieCharacter;
import com.moshi.webapi.services.FranchiseService.FranchiseService;
import com.moshi.webapi.services.MovieService.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
/**
 * This class helps to convert from Entity class to DTO and from DTO to Entity
 */
@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;
    @Autowired
    protected FranchiseService franchiseService;
    /**
     * converting from Entity to DTO
     * @param movieCharacter
     * @return
     */
    @Mapping(target = "movieIds", source = "movieIds", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO movieCharacterToMovieCharacterDTO(MovieCharacter movieCharacter);
    public abstract Collection<CharacterDTO> movieCharactersToMovieCharactersDTO(Collection<MovieCharacter> characters);
    /**
     * converting from Entity to DTO
     * @param characterDTO
     * @return
     */
    @Mapping(target = "movieIds", source = "movieIds", qualifiedByName = "moviesIdsToMovies")
    public abstract MovieCharacter movieCharacterDTOToMovieCharacter(CharacterDTO characterDTO);


    @Named("moviesIdsToMovies")
    List<Movie> mapIdsToMovies(List<Long> id) {
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toList());
    }

    @Named("moviesToIds")
    List<Long> mapMoviesToIds(List<Movie> movies) {
        if(movies == null)
            return null;
        return movies.stream()
                .map(Movie::getId).collect(Collectors.toList());
    }

}
