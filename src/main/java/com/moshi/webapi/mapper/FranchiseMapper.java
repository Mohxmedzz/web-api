package com.moshi.webapi.mapper;

import com.moshi.webapi.models.DTO.franchise.FranchiseDTO;
import com.moshi.webapi.models.Franchise;
import com.moshi.webapi.models.Movie;
import com.moshi.webapi.services.MovieCharacterService.MovieCharacterService;
import com.moshi.webapi.services.MovieService.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
/**
 * This class helps to convert from Entity class to DTO and from DTO to Entity
 */
@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;
    @Autowired
    protected MovieCharacterService movieCharacterService;
    /**
     * converting from Entity to DTO
     * @param franchise
     * @return
     */
    @Mapping(target = "movieIds", source = "movieIds", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchisesToFranchiseDTO(Collection<Franchise> franchises);
    /**
     * converting from DTO to Entity
     * @param dto
     * @return
     */
    @Mapping(target = "movieIds", source = "movieIds", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO dto);

    // Custom mappings
    @Named("movieIdsToMovies")
    List<Movie> mapIdsToMovies(List<Long> id) {
        return id.stream()
                .map(i -> movieService.findById(i))
                .collect(Collectors.toList());
    }

    @Named("moviesToIds")
    List<Long> mapMoviesToIds(List<Movie> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(Movie::getId).collect(Collectors.toList());
    }
}