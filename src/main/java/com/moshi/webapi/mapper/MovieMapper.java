package com.moshi.webapi.mapper;

import com.moshi.webapi.models.DTO.movie.MovieDTO;
import com.moshi.webapi.models.Franchise;
import com.moshi.webapi.models.Movie;
import com.moshi.webapi.models.MovieCharacter;
import com.moshi.webapi.services.MovieCharacterService.MovieCharacterService;
import com.moshi.webapi.services.FranchiseService.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Collection;

import java.util.List;
import java.util.stream.Collectors;
/**
 * This class helps to convert from Entity class to DTO and from DTO to Entity
 */
@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected MovieCharacterService movieCharacterService;
    @Autowired
    protected FranchiseService franchiseService;
    /**
     * converting from Entity to DTO
     * @param movie
     * @return
     */
    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "movieCharacters", source = "movieCharacters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDTO(Movie movie);
    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movie);
    /**
     * convert From DTO to Entity
     * @param movieDTO
     * @return
     */
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "movieCharacters", source = "movieCharacters", qualifiedByName = "charactersIdToCharacters")
    public abstract Movie movieDTOToMovie(MovieDTO movieDTO);

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(Long id) {
        return franchiseService.findById(id);
    }

    @Named("charactersIdToCharacters")
    List<MovieCharacter> mapIdsToCharacters(List<Long> id) {
        return id.stream()
                .map( i -> movieCharacterService.findById(i))
                .collect(Collectors.toList());
    }

    @Named("charactersToIds")
    List<Long> mapCharactersToIds(List<MovieCharacter> characters) {
        if(characters == null)
            return null;
        return characters.stream()
                .map(MovieCharacter::getId).collect(Collectors.toList());
    }
}
