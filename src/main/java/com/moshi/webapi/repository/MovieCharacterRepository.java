package com.moshi.webapi.repository;

import com.moshi.webapi.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MovieCharacterRepository extends JpaRepository<MovieCharacter, Long> {

}
